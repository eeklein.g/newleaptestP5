﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool isGrounded;
    private Rigidbody rbody;
    public Animator anim;
    public float jumpForce = 5f;

    private void Start()
    {
        isGrounded = true;
        rbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update ()
    {
        var rotation = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var translation = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        
        transform.Rotate(0, rotation, 0);
        transform.Translate(0, 0, translation);

        if(translation != 0)
        {
            if(Input.GetKeyDown(KeyCode.LeftShift))
            {
                anim.SetBool("isRunning", true);
                
            }
            else
            {
                anim.SetBool("isWalking", true);
              
            }
                
           
        }
        else
        {
            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning", false);
            
        }

        if(Input.GetKeyDown(KeyCode.K))
        {
            
            anim.SetTrigger("isShooting");
        }
        

        if (Input.GetButtonDown("Jump"))
        {
            rbody.velocity = new Vector3(0f, jumpForce, 0f);
            isGrounded = false;
            anim.SetTrigger("isJumping");
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            anim.SetBool("isGrounded", isGrounded);
        }
    }
}
